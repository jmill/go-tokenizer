package tokenizer

import (
	"crypto/rand"
	"fmt"
	"io"
	"sync"

	"gitlab.com/jmill/go-tokenizer/crypto"
)

const defaultTokenPrefix = `dp.token.`

var (
	// ErrTokenNotFound is returned when a supplied token is not found.
	ErrTokenNotFound = fmt.Errorf("token not found")
)

// DefaultTokenFunc generates a random token.
func DefaultTokenFunc() string {
	bytes := make([]byte, 32)
	io.ReadFull(rand.Reader, bytes)
	return fmt.Sprintf("%s%x", defaultTokenPrefix, bytes)
}

// InMemoryTokenizer implements the Tokenizer interface and stores all data
// in memory.
type InMemoryTokenizer struct {
	mu        sync.RWMutex
	secrets   Secrets
	crypto    crypto.EncrypterDecrypter
	tokenFunc func() string
}

// InMemoryTokenizerOption allows for optional configuration when calling
// NewInMemoryTokenizer.
type InMemoryTokenizerOption func(t *InMemoryTokenizer)

// WithTokenFunc sets the token generation function.
func WithTokenFunc(f func() string) InMemoryTokenizerOption {
	return func(t *InMemoryTokenizer) {
		t.tokenFunc = f
	}
}

// NewInMemoryTokenizer creates and returns a new InMemoryTokenizer.
func NewInMemoryTokenizer(c crypto.EncrypterDecrypter, opts ...InMemoryTokenizerOption) *InMemoryTokenizer {
	t := &InMemoryTokenizer{
		secrets:   Secrets{},
		crypto:    c,
		tokenFunc: DefaultTokenFunc,
	}

	for _, o := range opts {
		o(t)
	}

	return t
}

// Get implements Tokenizer.Get.
func (t *InMemoryTokenizer) Get(tokens ...string) (Secrets, error) {
	t.mu.RLock()
	defer t.mu.RUnlock()

	secrets := Secrets{}
	for _, token := range tokens {
		ciphertext, ok := t.secrets[token]
		if !ok {
			return nil, ErrTokenNotFound
		}

		plaintext, err := t.crypto.Decrypt([]byte(ciphertext))
		if err != nil {
			return nil, fmt.Errorf("failed to decrypt ciphertext: %w", err)
		}

		secrets[token] = string(plaintext)
	}

	return secrets, nil
}

// New implementes Tokenizer.New.
func (t *InMemoryTokenizer) New(value string) (string, error) {
	ciphertext, err := t.crypto.Encrypt([]byte(value))
	if err != nil {
		return "", fmt.Errorf("failed to encrypt plaintext: %w", err)
	}

	t.mu.Lock()
	defer t.mu.Unlock()

	var token string
	for {
		token = t.tokenFunc()
		if _, alreadyExists := t.secrets[token]; !alreadyExists {
			break
		}
	}

	t.secrets[token] = string(ciphertext)
	return token, nil
}

// Set implements Tokenizer.Set.
func (t *InMemoryTokenizer) Set(token, value string) error {
	t.mu.Lock()
	defer t.mu.Unlock()

	if _, ok := t.secrets[token]; !ok {
		return ErrTokenNotFound
	}

	ciphertext, err := t.crypto.Encrypt([]byte(value))
	if err != nil {
		return fmt.Errorf("failed to encrypt plaintext: %w", err)
	}

	t.secrets[token] = string(ciphertext)
	return nil
}

// Delete implements Tokenizer.Delete.
func (t *InMemoryTokenizer) Delete(token string) error {
	t.mu.Lock()
	defer t.mu.Unlock()

	delete(t.secrets, token)

	return nil
}
