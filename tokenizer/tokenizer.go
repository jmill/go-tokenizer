package tokenizer

// Tokenizer represents a tokenization service.
type Tokenizer interface {
	// Get returns the Secrets associated with the supplied tokens.
	Get(tokens ...string) (Secrets, error)

	// New encrypts and stores the supplied value. It returns a new token that
	// can be used to retrieve the stored value.
	New(value string) (string, error)

	// Set updates the stored value associated with the supplied token by
	// setting it to the supplied value.
	Set(token, value string) error

	// Delete deletes a stored token and its associated value.
	Delete(token string) error
}

// Secrets represents a collection of tokens and their associated values.
type Secrets map[string]string
