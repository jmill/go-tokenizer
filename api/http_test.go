package api

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/jmill/go-tokenizer/crypto"
	"gitlab.com/jmill/go-tokenizer/tokenizer"
)

func TestHTTPHandler_ServeHTTP(t *testing.T) {
	ctrl := gomock.NewController(t)
	encdec := crypto.NewMockEncrypterDecrypter(ctrl)
	srv := NewHTTPHandler(tokenizer.NewInMemoryTokenizer(encdec))

	var token1 string
	var token2 string

	t.Run(`creating a secret is successful`, func(t *testing.T) {
		encdec.EXPECT().Encrypt([]byte("hello")).Return([]byte("encrypted"), nil)
		res := executeTestRequest(srv, http.MethodPost, "/tokens", `{"secret": "hello"}`)
		assert.Equal(t, http.StatusCreated, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		token1 = body["token"]
		assert.True(t, len(token1) > 0)
		assert.True(t, strings.HasPrefix(token1, "dp.token."))
	})

	t.Run(`creating another secret is successful`, func(t *testing.T) {
		encdec.EXPECT().Encrypt([]byte("world")).Return([]byte("encrypted"), nil)
		res := executeTestRequest(srv, http.MethodPost, "/tokens", `{"secret": "world"}`)
		assert.Equal(t, http.StatusCreated, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		token2 = body["token"]
		assert.True(t, len(token2) > 0)
		assert.True(t, strings.HasPrefix(token2, "dp.token."))
	})

	t.Run(`ensure tokens are not equal`, func(t *testing.T) {
		assert.NotEqual(t, token1, token2)
	})

	t.Run(`reading token1 is successful`, func(t *testing.T) {
		encdec.EXPECT().Decrypt([]byte("encrypted")).Return([]byte("hello"), nil)
		res := executeTestRequest(srv, http.MethodGet, fmt.Sprintf("/tokens?t=%s", token1), ``)
		assert.Equal(t, http.StatusOK, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		assert.Equal(t, 1, len(body), body)
		assert.Equal(t, "hello", body[token1])
	})

	t.Run(`reading multiple tokens is successful`, func(t *testing.T) {
		encdec.EXPECT().Decrypt([]byte("encrypted")).Return([]byte("hello"), nil)
		encdec.EXPECT().Decrypt([]byte("encrypted")).Return([]byte("world"), nil)
		res := executeTestRequest(srv, http.MethodGet, fmt.Sprintf("/tokens?t=%s,%s", token1, token2), ``)
		assert.Equal(t, http.StatusOK, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		assert.Equal(t, 2, len(body), body)
		assert.Equal(t, "hello", body[token1])
		assert.Equal(t, "world", body[token2])
	})

	t.Run(`reading without t parameter yields 400`, func(t *testing.T) {
		res := executeTestRequest(srv, http.MethodGet, fmt.Sprintf("/tokens?x=%s", token1), ``)
		assert.Equal(t, http.StatusBadRequest, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		assert.Equal(t, 1, len(body), body)
		assert.Equal(t, "missing t query parameter", body["error"])
	})

	t.Run(`reading non-existent token yields 404`, func(t *testing.T) {
		res := executeTestRequest(srv, http.MethodGet, "/tokens?t=badtoken", ``)
		assert.Equal(t, http.StatusNotFound, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		assert.Equal(t, 1, len(body), body)
		assert.Equal(t, "token not found", body["error"])
	})

	t.Run(`update token is successful`, func(t *testing.T) {
		encdec.EXPECT().Encrypt([]byte("hola")).Return([]byte("encrypted"), nil)
		res := executeTestRequest(srv, http.MethodPut, fmt.Sprintf("/tokens/%s", token1), `{"secret": "hola"}`)
		assert.Equal(t, http.StatusOK, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		assert.Equal(t, 1, len(body), body)
		assert.Equal(t, token1, body["token"])
	})

	t.Run(`update non-existent token yields 404`, func(t *testing.T) {
		res := executeTestRequest(srv, http.MethodPut, "/tokens/badtoken", `{"secret": "hola"}`)
		assert.Equal(t, http.StatusNotFound, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		assert.Equal(t, 1, len(body), body)
		assert.Equal(t, "token not found", body["error"])
	})

	t.Run(`delete token is successful`, func(t *testing.T) {
		res := executeTestRequest(srv, http.MethodDelete, fmt.Sprintf("/tokens/%s", token1), ``)
		assert.Equal(t, http.StatusNoContent, res.StatusCode)

		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)
		assert.Equal(t, 0, len(body))

		// Ensure token was deleted
		res = executeTestRequest(srv, http.MethodGet, fmt.Sprintf("/tokens?t=%s", token1), ``)
		assert.Equal(t, http.StatusNotFound, res.StatusCode, `expected token to have been deleted`)
	})

	t.Run(`delete non-existent token is successful`, func(t *testing.T) {
		res := executeTestRequest(srv, http.MethodDelete, "/tokens/badtoken", ``)
		assert.Equal(t, http.StatusNoContent, res.StatusCode)

		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)
		assert.Equal(t, 0, len(body))
	})

	t.Run(`non-json update request body yields 400`, func(t *testing.T) {
		res := executeTestRequest(srv, http.MethodPut, fmt.Sprintf("/tokens/%s", token1), `this isn't json`)
		assert.Equal(t, http.StatusBadRequest, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		assert.Equal(t, 1, len(body), body)
		assert.Equal(t, "invalid JSON", body["error"])
	})

	t.Run(`non-json create request body yields 400`, func(t *testing.T) {
		res := executeTestRequest(srv, http.MethodPost, `/tokens`, `this isn't json`)
		assert.Equal(t, http.StatusBadRequest, res.StatusCode)

		body, err := getBodyJSON(res)
		require.NoError(t, err)
		assert.Equal(t, 1, len(body), body)
		assert.Equal(t, "invalid JSON", body["error"])
	})
}

func executeTestRequest(h http.Handler, method, path, body string) *http.Response {
	var bodyReader io.Reader
	if body != "" {
		bodyReader = strings.NewReader(body)
	}

	req := httptest.NewRequest(method, path, bodyReader)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, req)
	return w.Result()
}

func getBodyJSON(res *http.Response) (map[string]string, error) {
	defer res.Body.Close()
	dec := json.NewDecoder(res.Body)

	var result map[string]string
	return result, dec.Decode(&result)
}
