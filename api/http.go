package api

import (
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/jmill/go-tokenizer/tokenizer"
)

// HTTPHandler is a "net/http".Handler that provides a tokenization service
// HTTP API.
type HTTPHandler struct {
	tokens tokenizer.Tokenizer
	engine *gin.Engine
}

// ServeHTTP implements the "net/http".Handler interface to serve HTTP requests.
func (h *HTTPHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	h.engine.ServeHTTP(w, req)
}

// NewHTTPHandler returns a new HTTPHandler that provides an HTTP API to
// interact with the supplied Tokenizer t.
func NewHTTPHandler(t tokenizer.Tokenizer) *HTTPHandler {
	h := &HTTPHandler{tokens: t}

	g := gin.Default()
	g.GET("/tokens", h.read)
	g.POST("/tokens", h.write)
	g.PUT("/tokens/:token", h.update)
	g.DELETE("/tokens/:token", h.delete)

	h.engine = g

	return h
}

func (h *HTTPHandler) read(c *gin.Context) {
	tokenString := c.Query("t")
	if tokenString == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing t query parameter"})
		return
	}

	tokens := strings.Split(tokenString, ",")
	secrets, err := h.tokens.Get(tokens...)
	if err == tokenizer.ErrTokenNotFound {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "token not found"})
		return
	} else if err != nil {
		log.Printf("Error getting token: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, secrets)
}

func (h *HTTPHandler) write(c *gin.Context) {
	req := struct {
		Secret string `json:"secret"`
	}{}

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid JSON"})
		return
	}

	token, err := h.tokens.New(req.Secret)
	if err != nil {
		log.Printf("Failed to create token: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"token": token,
	})
}

func (h *HTTPHandler) update(c *gin.Context) {
	token := c.Params.ByName("token")
	req := struct {
		Secret string `json:"secret"`
	}{}

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid JSON"})
		return
	}

	err = h.tokens.Set(token, req.Secret)
	if err == tokenizer.ErrTokenNotFound {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "token not found"})
		return
	} else if err != nil {
		log.Printf("Failed to update token: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}

func (h *HTTPHandler) delete(c *gin.Context) {
	token := c.Params.ByName("token")

	err := h.tokens.Delete(token)
	if err != nil {
		log.Printf("Failed to delete token: %v", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.Status(http.StatusNoContent)
}
