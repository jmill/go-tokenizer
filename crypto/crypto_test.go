package crypto

import (
	"bytes"
	"crypto/rand"
	"io"
	"testing"
)

func TestAESEncrypterDecrypter(t *testing.T) {
	testCases := []struct {
		name      string
		plaintext []byte
	}{
		{
			name:      `Encrypt and data decrypt successfully`,
			plaintext: []byte("hello world"),
		},
		{
			name:      `Empty plaintext`,
			plaintext: []byte(""),
		},
		{
			name:      `Nil plaintext`,
			plaintext: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			key1 := make([]byte, 32)
			io.ReadFull(rand.Reader, key1)

			aes1 := NewAESEncrypterDecrypter(key1)
			ciphertext, err := aes1.Encrypt(tc.plaintext)
			if err != nil {
				t.Fatalf("unexpected error: '%v'", err)
			}

			if bytes.Equal(ciphertext, tc.plaintext) {
				t.Fatalf("expected ciphertext and plaintext to be different: '%v' == '%v'", ciphertext, tc.plaintext)
			}

			plaintext, err := aes1.Decrypt(ciphertext)
			if err != nil {
				t.Fatalf("unexpected error: '%v'", err)
			}

			if !bytes.Equal(plaintext, tc.plaintext) {
				t.Fatalf("expected decrypted plaintext and original plaintext to be the same: '%v' != '%v'", plaintext, tc.plaintext)
			}

			key2 := make([]byte, 32)
			io.ReadFull(rand.Reader, key2)

			aes2 := NewAESEncrypterDecrypter(key2)
			_, err = aes2.Decrypt(ciphertext)
			if err == nil {
				t.Fatal("expected cipher message authentication failure, but it was successful")
			}
		})
	}
}
