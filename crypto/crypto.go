package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
)

// EncrypterDecrypter encrypts and decrypts data.
//go:generate mockgen --build_flags=--mod=mod -destination=mock_crypto.go -package=crypto . EncrypterDecrypter
type EncrypterDecrypter interface {
	// Encrypt encrypts the supplied plaintext to ciphertext.
	Encrypt(plaintext []byte) ([]byte, error)
	// Decrypt decrypts the supplied ciphertext to plaintext.
	Decrypt(ciphertext []byte) ([]byte, error)
}

// AESEncrypterDecrypter implements EncrypterDecrypter using AES-256-GCM.
type AESEncrypterDecrypter struct {
	key []byte
}

// NewAESEncrypterDecrypter creates and returns a new AESEncrypterDecrypter.
// The key argument should be the AES key, either 16, 24, or 32 bytes to
// select AES-128, AES-192, or AES-256.
func NewAESEncrypterDecrypter(key []byte) *AESEncrypterDecrypter {
	return &AESEncrypterDecrypter{
		key: key,
	}
}

// Encrypt encrypts the supplied plaintext to ciphertext.
func (c *AESEncrypterDecrypter) Encrypt(plaintext []byte) ([]byte, error) {
	block, err := aes.NewCipher(c.key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	iv := make([]byte, gcm.NonceSize())
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}

	return gcm.Seal(iv, iv, plaintext, nil), nil
}

// Decrypt decrypts the supplied ciphertext to plaintext.
func (c *AESEncrypterDecrypter) Decrypt(ciphertext []byte) ([]byte, error) {
	block, err := aes.NewCipher(c.key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	iv := ciphertext[:gcm.NonceSize()]
	return gcm.Open(nil, iv, ciphertext[len(iv):], nil)
}
