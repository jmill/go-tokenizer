package main

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/jmill/go-tokenizer/api"
	"gitlab.com/jmill/go-tokenizer/crypto"
	"gitlab.com/jmill/go-tokenizer/tokenizer"
)

const (
	defaultServerTimeout = 5 * time.Second
)

type config struct {
	listenAddr    string
	aesKeyHexFile string
	genKey        bool
}

func main() {
	var c config
	flag.BoolVar(&c.genKey, "gen-key", false, "If `key-file` doesn't exist, create it and generate a new 256-bit key")
	flag.StringVar(&c.listenAddr, "addr", ":8080", "Address on which to listen for HTTP requests")
	flag.StringVar(&c.aesKeyHexFile, "key-file", "token.key", "Path to file containing hexadecimal AES key string; 16 | 24 | 32 bytes to select AES-128 | AES-192 | AES-256")
	flag.Parse()

	if err := run(c); err != nil {
		log.Fatal(err)
	}
}

func run(c config) error {
	key, err := getKey(c)
	if err != nil {
		return err
	}

	t := tokenizer.NewInMemoryTokenizer(crypto.NewAESEncrypterDecrypter(key))

	srv := http.Server{
		Addr:              c.listenAddr,
		Handler:           api.NewHTTPHandler(t),
		ReadHeaderTimeout: defaultServerTimeout,
		WriteTimeout:      defaultServerTimeout,
	}

	// Handle SIGINT & SIGTERM for graceful shutdown
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	go func() {
		<-ctx.Done()
		shutdownCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		srv.Shutdown(shutdownCtx)
	}()

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		return err
	}

	return nil
}

func getKey(c config) ([]byte, error) {
	file, err := os.Open(c.aesKeyHexFile)
	if c.genKey && errors.Is(err, fs.ErrNotExist) {
		return createKeyFile(c.aesKeyHexFile)
	} else if err != nil {
		return nil, err
	}
	defer file.Close()

	key, err := io.ReadAll(hex.NewDecoder(file))
	if err != nil {
		return nil, fmt.Errorf("keyfile '%s' does not contain valid hexadecimal: %w", c.aesKeyHexFile, err)
	} else if keyLen := len(key); keyLen != 16 && keyLen != 24 && keyLen != 32 {
		return nil, fmt.Errorf("key length was %d bytes but must be either 16, 24, or 32 bytes", keyLen)
	}

	return key, nil
}

func createKeyFile(name string) (key []byte, err error) {
	key = make([]byte, 32)
	io.ReadFull(rand.Reader, key)

	file, err := os.OpenFile(name, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return nil, fmt.Errorf("failed to create key file: %v", err)
	}
	defer file.Close()

	_, err = fmt.Fprintf(file, "%x", key)
	return key, err
}
